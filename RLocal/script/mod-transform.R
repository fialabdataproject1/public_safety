source("~/public_safety/script/mod-load.R")

# Determina as transformações da base de dados.
#' 
#' @param d_crime, recebe a base de dados.
#'  
#' @export

f_transform_dataset_crimes <- function(d_crime){
  # Remove as variáveis desnecessárias.
  
  d_crime <- d_crime[-c(2, 5, 10, 13:16, 18)] 
  
  #  Altera o tipo das variáveis e remove os espaços em brancos.
  
  d_crime$beat <- as.integer(trimws(d_crime$beat))
  d_crime$district <- as.integer(trimws(d_crime$district))
  d_crime$block <- trimws(d_crime$block)
  d_crime$primary_type <- trimws(d_crime$primary_type)
  d_crime$description <- trimws(d_crime$description)
  d_crime$location_description <- trimws(d_crime$location_description)
  d_crime$arrest <- as.factor(trimws(d_crime$arrest))
  d_crime$year <- trimws(d_crime$year)
  d_crime$month <- substring(trimws(d_crime$date), 6, 7)

  # Reorganiza as colunas.
  
  d_crime <- d_crime[c(1, 2, 10, 13, 3, 6, 4, 5, 7, 8, 9, 11, 12)]
}
